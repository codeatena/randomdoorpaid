//
//  ShareViewController.h
//  RandomDoor
//
//  Created by JinSung Han on 5/31/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"

@interface ShareViewController : UIViewController <UIDocumentInteractionControllerDelegate>
{
    UIDocumentInteractionController *docController;
    GADBannerView*  bannerView_;
}

@property(nonatomic, retain) UIViewController* parentViewCtrl;
@property(nonatomic, retain) UIImage* bmpFullImage;

- (IBAction)onBack:(id)sender;
- (IBAction)onFacebook:(id)sender;
- (IBAction)onTwitter:(id)sender;
- (IBAction)onInstagram:(id)sender;
- (IBAction)onCancel:(id)sender;

@end