//
//  MainViewController.m
//  RandomDoor
//
//  Created by JinSung Han on 2/28/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "MainViewController.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import <Social/Social.h>
#import "FullViewController.h"
#import "AppDelegate.h"
#import "Common.h"

#define MAX_TIME_READY_OPEN     86400.0f
//#define MAX_TIME_READY_OPEN     15.0f
#define READY_TIME_OPEN         0.0f

#define DURATION_TIME_OPEN      1.0f
#define DELAY_TIME_CLOSE        3.0f
#define DURATION_TIME_CLOSE     0.45f
#define DURATION_SHIRINK_TIME   0.06f
#define SHIRINK_DELTA_X 10.0f
#define SECRET_KEY              @"73e4d8e848405a88f444cff1c9dbc5b8"

#define SERVER_URL              @"http://random-door.com/"
#define FILE_PATH               @"getAPI.php"

#define ACTION_IMAGE_BEHIND_DOOR @"image_behind_door"
#define ACTION_IMAGE_RANDOM_MARK @"image_random_mark"
#define ACTION_IMAGE_OPEN_DOOR_ANIMATE @"image_open_door_animate"
#define ACTION_IMAGE_CLOSE_DOOR_ANIMATE @"image_close_door_animate"

#define OPEN_ANIMATION_FRAME_COUNT          9
#define CLOSE_ANIMATION_FRAME_COUNT         9

#define MAIN_BANNER_UNIT_ID       @"ca-app-pub-5533489369819223/5073074396"

@interface MainViewController ()

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [NSTimer scheduledTimerWithTimeInterval: 1.0f target: self selector:@selector(calcReadyDoor) userInfo: nil repeats: YES];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(openDoor:)];
    singleTap.numberOfTapsRequired = 1;
    [m_imgDoorBack addGestureRecognizer:singleTap];
    
    m_lblReadyTime.layer.zPosition = 1001;
//    inImage = [[UIImage alloc] init];
    
    [NSTimer scheduledTimerWithTimeInterval: 3.0f target: self selector:@selector(setBehindDoorImage) userInfo: nil repeats: YES];
    
//    [self responseImagePath];
    [self calcReadyDoor];
    [self setOpenAnimateImage];
    [self setCloseAnimateImage];
    [self setRandomDoorMarkImage];
    [self setBehindDoorImage];
    
//    bannerView_ = [[GADBannerView alloc]
//                   initWithFrame:CGRectMake(0.0, 518, GAD_SIZE_320x50.width,                                   GAD_SIZE_320x50.height)];//设置位置
//    
//    if ([Common getDeviceType] == DEVICE_TYPE_IPHONE_RETINA_35_INCH) {
//        bannerView_.frame = CGRectMake(0, 430, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
//    }
//    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
//    bannerView_.adUnitID = MAIN_BANNER_UNIT_ID;//调用你的id
//    
//    // Let the runtime know which UIViewController to restore after taking
//    // the user wherever the ad goes and add it to the view hierarchy.
//    bannerView_.rootViewController = self;
////    bannerView_.backgroundColor = [UIColor redColor];
//    [self.view addSubview:bannerView_];//添加bannerview到你的试图
//    [bannerView_ loadRequest:[GADRequest request]];
    isOpenDoor = NO;
    
    [self initSound];
}

- (void) initSound
{
    NSURL *urlOpen = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"door_open" ofType:@"mp3"]];
    NSURL *urlShut = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"door_shut" ofType:@"mp3"]];
    NSURL *urlLock = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"door_lock" ofType:@"mp3"]];

    NSError *error = nil;
    
    plyOpen = [[AVAudioPlayer alloc] initWithContentsOfURL:urlOpen error:&error];
    plyShut = [[AVAudioPlayer alloc] initWithContentsOfURL:urlShut error:&error];
    plyLock = [[AVAudioPlayer alloc] initWithContentsOfURL:urlLock error:&error];
    
    [plyOpen prepareToPlay];
    [plyShut prepareToPlay];
    [plyLock prepareToPlay];
}

- (void) setRandomDoorMarkImage
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:ACTION_IMAGE_RANDOM_MARK forKey:@"action"];
    [parameters setObject:SECRET_KEY forKey:@"key"];
    [parameters setObject:@"12345" forKey:@"unique-ID"];
    
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:SERVER_URL]];
    
    NSURLRequest *urlRequest = [client requestWithMethod:@"POST" path:FILE_PATH parameters:(NSDictionary *)parameters] ;
    AFHTTPRequestOperation *myOperation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
    [myOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *strResponse = operation.responseString;
//        NSLog(@"mark image: %@", strResponse);
        
        NSData *data = [[NSData alloc]initWithBase64EncodedString:strResponse options:NSDataBase64DecodingIgnoreUnknownCharacters];
        m_imgMark.image = [UIImage imageWithData:data];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
    }];
    
    [[[NSOperationQueue alloc] init] addOperation:myOperation];
}

- (void) setOpenAnimateImage
{
    [g_delegate showWaitingScreen:@"Loading.." bShowText:YES];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:ACTION_IMAGE_OPEN_DOOR_ANIMATE forKey:@"action"];
    [parameters setObject:SECRET_KEY forKey:@"key"];
    [parameters setObject:@"12345" forKey:@"unique-ID"];
    
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:SERVER_URL]];
    
    NSURLRequest *urlRequest = [client requestWithMethod:@"POST" path:FILE_PATH parameters:(NSDictionary *)parameters] ;
    AFHTTPRequestOperation *myOperation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
    [myOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *strResponse = operation.responseString;
        NSLog(@"OPEN ANIMATE image: %@", strResponse);
        
        NSData *data = [[NSData alloc]initWithBase64EncodedString:strResponse options:NSDataBase64DecodingIgnoreUnknownCharacters];
        UIImage* image = [UIImage imageWithData:data];
        
        bmpReadyDoor = [self getSubImageFrom:image WithRect:CGRectMake(0, 0, 298, 531)];
        if (!isLockedDoor) {
            m_imgDoorBack.image = bmpReadyDoor;
        } else {
            m_imgDoorBack.image = bmpLockedDoor;
        }
        
        arrOpenAnimationImage = [[NSMutableArray alloc] init];
        for (int i = 0; i < OPEN_ANIMATION_FRAME_COUNT; i++) {
            UIImage* imgFrame = [self getSubImageFrom:image WithRect:CGRectMake(i * 298, 0, 298, 531)];
            [arrOpenAnimationImage addObject:imgFrame];
        }
        
        [g_delegate hideWaitingScreen];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
    }];
    
    [[[NSOperationQueue alloc] init] addOperation:myOperation];
}

- (void) setCloseAnimateImage
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:ACTION_IMAGE_CLOSE_DOOR_ANIMATE forKey:@"action"];
    [parameters setObject:SECRET_KEY forKey:@"key"];
    [parameters setObject:@"12345" forKey:@"unique-ID"];
    
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:SERVER_URL]];
    
    NSURLRequest *urlRequest = [client requestWithMethod:@"POST" path:FILE_PATH parameters:(NSDictionary *)parameters] ;
    AFHTTPRequestOperation *myOperation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
    [myOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *strResponse = operation.responseString;
//        NSLog(@"CLOSE ANIMATE image: %@", strResponse);
        
        NSData *data = [[NSData alloc]initWithBase64EncodedString:strResponse options:NSDataBase64DecodingIgnoreUnknownCharacters];
        UIImage* image = [UIImage imageWithData:data];
        
        bmpLockedDoor = [self getSubImageFrom:image WithRect:CGRectMake(0, 0, 298, 531)];
        
        if (!isLockedDoor) {
            m_imgDoorBack.image = bmpReadyDoor;
        } else {
            m_imgDoorBack.image = bmpLockedDoor;
        }
        
        arrCloseAnimationImage = [[NSMutableArray alloc] init];
        for (int i = CLOSE_ANIMATION_FRAME_COUNT - 1; i >= 0; i--) {
            UIImage* imgFrame = [self getSubImageFrom:image WithRect:CGRectMake(i * 298, 0, 298, 531)];
            [arrCloseAnimationImage addObject:imgFrame];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
    }];
    
    [[[NSOperationQueue alloc] init] addOperation:myOperation];
}

- (UIImage *) getSubImageFrom:(UIImage *)imageToCrop WithRect:(CGRect)rect {
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

- (void) setBehindDoorImage
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:ACTION_IMAGE_BEHIND_DOOR forKey:@"action"];
    [parameters setObject:SECRET_KEY forKey:@"key"];
    [parameters setObject:@"12345" forKey:@"unique-ID"];
    
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:SERVER_URL]];
    
    NSURLRequest *urlRequest = [client requestWithMethod:@"POST" path:FILE_PATH parameters:(NSDictionary *)parameters] ;
    AFHTTPRequestOperation *myOperation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
    [myOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *strResponse = operation.responseString;
//        NSLog(@"BEHIND DOOR IMAGE: %@", strResponse);
        
        NSData *data = [[NSData alloc]initWithBase64EncodedString:strResponse options:NSDataBase64DecodingIgnoreUnknownCharacters];
        bmpBehindDoor = [UIImage imageWithData:data];
        
//        m_imgDoorOpened.image = bmpBehindDoor;

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
    }];
    
    [[[NSOperationQueue alloc] init] addOperation:myOperation];
}

- (void) calcReadyDoor
{
    double currentTime = [[NSDate date] timeIntervalSince1970];
//    NSLog(@"current time : %lf", currentTime);
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    double lastOpenDoorTime = [userDefaults doubleForKey: @"lastOpenDoorTime"];
//    NSLog(@"lastOpenDoorTime : %lf", lastOpenDoorTime);
    
    double diffTime = currentTime - lastOpenDoorTime;
//    NSLog(@"different Time : %lf", diffTime);

    if (diffTime < READY_TIME_OPEN) {
        isLockedDoor = YES;
        
        int k = (int) 86400 - (int) diffTime;
        
        int h = k / 3600;
        int m = (k - (h * 3600)) / 60;
        int s = k % 60;
//        NSString *str = [NSString stringWithFormat: @"CLOSED! Check Back\n in %d:%d:%d", h, m, s];
        
        NSString *str = [NSString stringWithFormat:@"%d", (int) (READY_TIME_OPEN - diffTime + 1.0f)];
        [m_lblReadyTime setText: str];
//        m_lblReadyText.hidden = NO;
//        m_lblReadyTime.hidden = NO;
    } else {
        isLockedDoor = NO;
        m_lblReadyTime.hidden = YES;
        m_lblReadyText.hidden = YES;
//        m_imgDoorClosed.image = [UIImage imageNamed: @"door.png"];
    }
}

- (void) openDoor: (UITapGestureRecognizer *) recognizer
{
//    [self closeDoor];
//    return;
    CGPoint point;
    if (recognizer.state == UIGestureRecognizerStateEnded)
    {
//        point = [recognizer locationInView: m_imgDoorClosed];
        NSLog(@"%@", NSStringFromCGPoint(point));
    }
    
    if (isLockedDoor || isOpenDoor) {
        [self shirinkDoor];
        return;
    }
    
//    if (point.x < 182.0f) return;
    
    [plyOpen play];
    
    isOpenDoor = YES;
    NSLog(@"action opendoor");
    
    m_imgDoorOpened.image = bmpBehindDoor;

    m_imgDoorBack.image = [arrOpenAnimationImage objectAtIndex: OPEN_ANIMATION_FRAME_COUNT - 1];
    m_imgDoorBack.animationDuration = DURATION_TIME_OPEN;
    m_imgDoorBack.animationImages = arrOpenAnimationImage;
    m_imgDoorBack.animationRepeatCount = 1;

    [m_imgDoorBack startAnimating];
    
    [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector: @selector(stopOpenDoorAnimation) userInfo:nil repeats:NO];
//    [m_imgDoorBack stopAnimating];
//    m_imgDoorClosed.layer.zPosition = 1000;
    
//    [NSTimer scheduledTimerWithTimeInterval:DURATION_TIME_CLOSE target:self selector: @selector(hideDoor) userInfo:nil repeats:NO];
//    [NSTimer scheduledTimerWithTimeInterval:DURATION_TIME_CLOSE + DELAY_TIME_CLOSE target:self selector: @selector(closeDoor) userInfo:nil repeats:NO];

//    double currentTime = [[NSDate date] timeIntervalSince1970];
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    [userDefaults setDouble: currentTime forKey: @"lastOpenDoorTime"];
//    [userDefaults synchronize];
    
    
//    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
//    animation.toValue = @(-M_PI_2);
//    animation.repeatCount = 1;
//    animation.duration = DURATION_TIME_CLOSE;
//    
//    [m_imgDoorClosed.layer addAnimation:animation forKey:@"rotation"];
//    
//    CATransform3D transform = CATransform3DIdentity;
//    transform.m34 = 1.0 / 500.0;
//    m_imgDoorClosed.layer.transform = transform;
}

- (void) closeDoor
{
    NSLog(@"Close Door");
    //    m_imgDoorClosed.hidden = NO;
    //    m_imgDoorClosed.image = [UIImage imageNamed: @"door_complete.png"];
    
    //    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    //    rotationAnimation.fromValue = [NSNumber numberWithFloat: -M_PI / 2];
    //    rotationAnimation.toValue = [NSNumber numberWithFloat: 0];
    //    rotationAnimation.duration = DURATION_TIME_CLOSE;
    
    //    [m_imgDoorClosed.layer addAnimation:rotationAnimation forKey:@"rotationAnimation1"];
    
    
    [NSTimer scheduledTimerWithTimeInterval:DURATION_TIME_CLOSE target:self selector: @selector(showReadyTime) userInfo:nil repeats:NO];

    m_imgDoorBack.image = bmpReadyDoor;
    m_imgDoorBack.animationDuration = DURATION_TIME_CLOSE;
    m_imgDoorBack.animationImages = arrCloseAnimationImage;
    m_imgDoorBack.animationRepeatCount = 1;
    
    [m_imgDoorBack startAnimating];
    
    [plyShut play];
}

- (void) stopOpenDoorAnimation
{
//    [m_imgDoorBack stopAnimating];
    FullViewController* fullViewCtrl = [[FullViewController alloc] initWithNibName:@"FullViewController" bundle:nil];
    [self presentViewController:fullViewCtrl animated:YES completion:nil];
    fullViewCtrl.parentViewCtrl = self;
    [fullViewCtrl setImage:m_imgDoorOpened.image];
}

- (void) shirinkDoor
{
    [plyLock play];
    [self performSelector: @selector(leftSmallMove) withObject: nil afterDelay: 0.0f];
    
    int k = 5;
    for (int i = 0; i < k; i++) {
        
        float d1 = DURATION_SHIRINK_TIME + DURATION_SHIRINK_TIME * 2 * i * 2;
        float d2 = DURATION_SHIRINK_TIME + DURATION_SHIRINK_TIME * 2 * (i * 2 + 1);
        [self performSelector: @selector(rightLargeMove) withObject: nil afterDelay: d1];
        [self performSelector: @selector(leftLargeMove) withObject: nil afterDelay: d2];
    }
    
    [self performSelector: @selector(rightLargeMove) withObject: nil afterDelay: DURATION_SHIRINK_TIME + DURATION_SHIRINK_TIME * 2 * k * 2];
    [self performSelector: @selector(leftSmallMove) withObject: nil afterDelay: DURATION_SHIRINK_TIME + DURATION_SHIRINK_TIME * 2 * (k * 2 + 1)];
}

- (void) leftSmallMove
{
    [self moveDoorAnimation: SHIRINK_DELTA_X DURATION: DURATION_SHIRINK_TIME];
}

- (void) leftLargeMove
{
    [self moveDoorAnimation: SHIRINK_DELTA_X * 2 DURATION: DURATION_SHIRINK_TIME * 2];
}

- (void) rightLargeMove
{
    [self moveDoorAnimation: -SHIRINK_DELTA_X * 2 DURATION: DURATION_SHIRINK_TIME * 2];
}

- (void) moveDoorAnimation: (float) dx DURATION: (float) duration
{
    CGPoint curPos = m_viewDoor.center;
    CGPoint newPos = CGPointMake(curPos.x + dx, curPos.y);
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    m_viewDoor.center = newPos;
    [UIView commitAnimations];
}

- (void) hideDoor
{
//    m_imgDoorClosed.hidden = YES;
}

- (void) showReadyTime
{
    double currentTime = [[NSDate date] timeIntervalSince1970];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setDouble: currentTime forKey: @"lastOpenDoorTime"];
    [userDefaults synchronize];
    
    isOpenDoor = NO;
//    isLockedDoor = YES;

//    m_lblReadyTime.text = @"6";
//    m_lblReadyTime.hidden = NO;
//    m_lblReadyText.hidden = NO;
    
    [self setLocalNotification];
//    [self responseImagePath];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (IBAction)onFacebook:(id)sender
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *faceSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [faceSheet addImage:bmpBehindDoor];
        [self presentViewController:faceSheet animated:YES completion:nil];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Information!" message:@"Your phone doesnt have the facebook app installed. Download the facebook app to share random-door on facebook." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (IBAction)onTwitter:(id)sender
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet addImage:bmpBehindDoor];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Information!" message:@"Your phone doesnt have the twitter app installed. Download the Twitter app to share random-door on twitter." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (IBAction)onInstagram:(id)sender
{
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"Image.igo"];
        
        NSData *data = UIImagePNGRepresentation(bmpBehindDoor);
        [data writeToFile:savedImagePath atomically:YES];
        
        NSURL *imageUrl = [NSURL fileURLWithPath:savedImagePath];
        
        if (docController == NULL) {
            docController = [[UIDocumentInteractionController alloc] init];
            docController.delegate = self;
        }
        
        docController.UTI = @"com.instagram.photo";
        docController.URL = imageUrl;
        [docController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
    }
    else
    {
        UIAlertView *errorToShare = [[UIAlertView alloc] initWithTitle:@"Information!" message:@"Your phone doesnt have the instagram app installed. Download the instagram app to share random-door on instagram." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorToShare show];
    }
}

- (void) setLocalNotification
{
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        [app cancelLocalNotification:oneEvent];
    }
    
    NSDate *currentDate = [NSDate date];
    NSDate *fireDate = [currentDate dateByAddingTimeInterval:(MAX_TIME_READY_OPEN)];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = fireDate;
    localNotification.alertBody = @"Have you checked out the random door today?";
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

@end
