//
//  FullViewController.h
//  RandomDoor
//
//  Created by JinSung Han on 5/31/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareViewController.h"

@interface FullViewController : UIViewController
{
    IBOutlet UIImageView* imgFull;
    ShareViewController* shareViewCtrl;
}

@property (nonatomic, retain) UIViewController* parentViewCtrl;

- (void) setImage: (UIImage*) image;
- (void) hide;

@end
