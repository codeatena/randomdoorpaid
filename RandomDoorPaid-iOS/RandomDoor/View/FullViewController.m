//
//  FullViewController.m
//  RandomDoor
//
//  Created by JinSung Han on 5/31/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "FullViewController.h"
#import "ShareViewController.h"
#import "MainViewController.h"

@interface FullViewController ()

@end

@implementation FullViewController

@synthesize parentViewCtrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(openShare:)];
    singleTap.numberOfTapsRequired = 1;
    [imgFull addGestureRecognizer:singleTap];
    
    shareViewCtrl = [[ShareViewController alloc] initWithNibName:@"ShareViewController" bundle:nil];
    shareViewCtrl.parentViewCtrl = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setImage: (UIImage*) image
{
    imgFull.image = image;
    shareViewCtrl.bmpFullImage = image;
}

- (void) openShare: (UITapGestureRecognizer *) recognizer
{
    NSLog(@"openShare");
    [self.view addSubview:shareViewCtrl.view];
}

- (void) hide
{
    [self dismissViewControllerAnimated:YES completion:^{[self completeDissmiss];}];

}

- (void) completeDissmiss
{
    MainViewController* mainViewCtrl = (MainViewController*) parentViewCtrl;
    [mainViewCtrl closeDoor];
}

@end
