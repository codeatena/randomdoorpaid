//
//  MainViewController.h
//  RandomDoor
//
//  Created by JinSung Han on 2/28/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"
#import <AVFoundation/AVFoundation.h>

@interface MainViewController : UIViewController <UIDocumentInteractionControllerDelegate>
{
    IBOutlet    UIImageView*        m_imgDoorOpened;
    IBOutlet    UIImageView*        m_imgDoorBack;
    
    IBOutlet    UILabel*            m_lblReadyText;
    IBOutlet    UILabel*            m_lblReadyTime;
    
    IBOutlet    UIImageView*        m_imgMark;
    
    IBOutlet    UIView*             m_viewDoor;
    
    long lLastOpenTime;
    
    BOOL isOpenDoor;
    BOOL isLockedDoor;
    
    UIImage*                        bmpReadyDoor;
    UIImage*                        bmpLockedDoor;
    UIImage*                        bmpBehindDoor;
    
    NSMutableArray*                 arrOpenAnimationImage;
    NSMutableArray*                 arrCloseAnimationImage;
    
    AVAudioPlayer*                  plyOpen;
    AVAudioPlayer*                  plyShut;
    AVAudioPlayer*                  plyLock;

    NSData * imageData;
    
    UIDocumentInteractionController *docController;
    
    GADBannerView*  bannerView_;
}

- (void) calcReadyDoor;

- (IBAction)onFacebook:(id)sender;
- (IBAction)onTwitter:(id)sender;
- (IBAction)onInstagram:(id)sender;

- (void) closeDoor;

@end
