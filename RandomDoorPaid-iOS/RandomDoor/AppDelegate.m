//
//  AppDelegate.m
//  RandomDoor
//
//  Created by JinSung Han on 2/28/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "Common.h"

extern AppDelegate* g_delegate;

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    g_delegate = self;
    
    MainViewController* mainViewCtrl = [[MainViewController alloc] initWithNibName: @"MainViewController" bundle: nil];
    [self.window setRootViewController:mainViewCtrl];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)showWaitingScreen:(NSString *)strText  bShowText:(BOOL)bShowText {
    
    UIView *view = [[UIView alloc] init];
    
    if ([Common is_Phone]) {
        if ([Common is_Retina]) {
            if ([Common getDeviceType] == DEVICE_TYPE_IPHONE_RETINA_35_INCH) {
                [view setFrame:CGRectMake(0, 0, 320, 480)];
            }
            else {
                [view setFrame:CGRectMake(0, 0, 320, 568)];
            }
        }
        else {
            [view setFrame:CGRectMake(0, 0, 320, 480)];
        }
    }
    else {
        if ([Common is_Retina]) {
            [view setFrame:CGRectMake(0, 0, 768, 1024)];
        }
        else {
            [view setFrame:CGRectMake(0, 0, 768, 1024)];
        }
    }
    
    [view setTag:TAG_WAIT_SCREEN_VIEW];
    [view setBackgroundColor:[UIColor clearColor]];
    [view setAlpha:1.0f];
    
    if (bShowText) {
        UIView *subView = [[UIView alloc] init];
        [subView setBackgroundColor:[UIColor blackColor]];
        [subView setAlpha:0.6];
        
        int width = 0;
        int height = 0;
        
        if ([Common is_Phone]) {
            width = 150;
            height = 100;
        }
        else {
            width = 300;
            height = 200;
        }
        
        [subView setFrame:CGRectMake(view.frame.size.width/2-width/2, view.frame.size.height/2-height/2, width, height)];
        
        UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [indicatorView setTag:TAG_WAIT_SCREEN_INDICATOR];
        
        CGRect rectIndicatorViewFrame = [indicatorView frame];
        
        width = rectIndicatorViewFrame.size.width;
        height = rectIndicatorViewFrame.size.height;
        
        [indicatorView setFrame:CGRectMake(subView.frame.size.width/2-width/2, subView.frame.size.height/3-width/2, width, height)];
        
        [indicatorView startAnimating];
        [subView addSubview:indicatorView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, subView.frame.size.height * 2/3, subView.frame.size.width, subView.frame.size.height/3)];
        [label setText:strText];
        
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextAlignment:NSTextAlignmentCenter];
        
        [label setTextColor:[UIColor whiteColor]];
        [label setTag:TAG_WAIT_SCREEN_LABEL];
        
        if ([Common is_Phone] ) {
            [label setFont:[UIFont systemFontOfSize:17.0f]];
        }
        else {
            [label setFont:[UIFont systemFontOfSize:34.0f]];
        }
        
        [subView addSubview:label];
        subView = [Common roundCornersOnView:subView onTopLeft:YES topRight:YES bottomLeft:YES bottomRight:YES radius:10.0f];
        
        view.userInteractionEnabled = YES;
        view.layer.zPosition = 100;
        [view addSubview:subView];
    }
    
    [_window.rootViewController.view addSubview:view];
}

- (void)hideWaitingScreen {
    
    UIView *view = [_window viewWithTag:TAG_WAIT_SCREEN_VIEW];
    
    if (view) {
        UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView*)[view viewWithTag:TAG_WAIT_SCREEN_INDICATOR];
        
        if (indicatorView)
            [indicatorView stopAnimating];
        
        [view removeFromSuperview];
        
        UILabel *label = (UILabel *)[view viewWithTag:TAG_WAIT_SCREEN_LABEL];
        if (label) {
            [label removeFromSuperview];
        }
    }
}

@end
