//
//  Common.h
//  mobitalent
//
//  Created by wang chenglei on 12/3/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DEVICE_TYPE_IPHONE                      0
#define DEVICE_TYPE_IPHONE_RETINA_35_INCH       1
#define DEVICE_TYPE_IPHONE_RETINA_4_INCH        2
#define DEVICE_TYPE_IPAD                        3
#define DEVICE_TYPE_IPAD_RETINA                 4

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define TAG_WAIT_SCREEN_VIEW            1025
#define TAG_WAIT_SCREEN_INDICATOR       1026
#define TAG_WAIT_SCREEN_LABEL           1027

typedef enum SocialAccountType {
    SocialAccountTypeFacebook = 1,
    SocialAccountTypeTwitter = 2
} SocialAccountType;

@interface Common : NSObject

+ (float)getScreenHeight;
+ (int)getDeviceType;

+ (BOOL)is_Retina;
+ (BOOL)is_Phone;

+ (NSString*)getXibNameWithSuffix:(NSString*)xibName;
+ (NSString*)getXibNameWithSuffixWithoutVersion:(NSString*)xibName;

+ (NSString *)getCellXibNameWithSuffix:(NSString *)xibName;

+ (UIImage *)resizeImage:(UIImage *)image newSize:(CGSize)newSize;
+ (NSString*)getStringFromDiffDate:(double)time standardTime:(double)standardTime;

+ (NSData *)makeThumbFromFilePath:(NSString *)videoPath;
+ (NSData *)makeThumbFromFilePathWithRange:(NSString *)videoPath startTime:(double)startTime duration:(double)duration;
+ (NSData *)makeThumbFromURL:(NSString *)videoURL;

+ (NSString *)makeIntNumber:(int)number;
+ (NSString *)makeIntNumberUsingThousandUnit:(int)number;

+(UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius;

@end
