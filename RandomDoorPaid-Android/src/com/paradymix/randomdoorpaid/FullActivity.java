package com.paradymix.randomdoorpaid;

import java.util.Timer;
import java.util.TimerTask;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.view.GestureDetector;

public class FullActivity extends Activity implements GestureDetector.OnGestureListener, 
		GestureDetector.OnDoubleTapListener {
	
	public static ImageView imgFull;
	private GestureDetectorCompat mDetector;
	private AdView adView;
	public ShareDialog dlgShare;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_full);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		imgFull = (ImageView) findViewById(R.id.full_imageView);
		imgFull.setImageBitmap(MainActivity.bmpBehindDoorTemp);
		
		mDetector = new GestureDetectorCompat(this, this);
		mDetector.setOnDoubleTapListener(this);
		
//		LinearLayout llytAdView = (LinearLayout) findViewById(R.id.adview_linearLayout);
//		adView = new AdView(this, AdSize.BANNER, "a15070abc92c05e");;
//		AdRequest re = new AdRequest();
//		re.setTesting(true);
//		adView.loadAd(re);
		
//		llytAdView.addView(adView);		
		dlgShare = new ShareDialog(this, "Share");
	}
	
	private void initValue() {

	}
	
	private void initEvent() {
		imgFull.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
//				ShareDialog dlgShare = new ShareDialog(FullActivity.this, "Share");
				dlgShare.show();
			}
        });
	}
	
	public void onBackPressed() {
		
	}
	
	@Override 
    public boolean onTouchEvent(MotionEvent event){ 
        this.mDetector.onTouchEvent(event);
        // Be sure to call the superclass implementation
        return super.onTouchEvent(event);
    }
	
    @Override
    public boolean onDown(MotionEvent event) { 
        Log.e("gesture", "onDown"); 
        return true;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2, 
            float velocityX, float velocityY) {
    	Log.e("gesture", "onFling");
        return true;
    }

    @Override
    public void onLongPress(MotionEvent event) {
    	Log.e("gesture", "onLongPress"); 
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, 
              float distanceX, float distanceY) {
    	Log.e("gesture", "onScroll");
        return true;
    }
  
    @Override
    public void onShowPress(MotionEvent event) {
    	Log.e("gesture", "onShowPress");
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
    	Log.e("gesture", "onSingleTapUp");
    	
//		Timer timer = new Timer();
//		timer.schedule(new TimerTask() {
//	        @Override
//	        public void run() {
//	        	FullActivity.this.runOnUiThread(new Runnable() {
//	        		public void run() {
//	    	        	Dialog dlgShare = new ShareDialog(FullActivity.this, "Share");
//	    	        	dlgShare.show();
//	        		}
//	        	});
//	        }
//		}, 100);
		
        return true;
    }
  
    @Override
    public boolean onDoubleTap(MotionEvent event) {
    	Log.e("gesture", "onDoubleTap");
        return true;
    }
  
    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {
    	Log.e("gesture", "onDoubleTapEvent");
//    	finish();
        return true;
    }
  
    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
    	Log.e("gesture", "onSingleTapConfirmed");
        return true;
    }
}
