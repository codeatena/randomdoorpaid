package com.paradymix.randomdoorpaid;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class MyService extends Service {
	public SharedPreferences sharedPreferences;
		
	String tag="HCW";
   	@Override
   	public void onCreate() {
   		super.onCreate();
   		Toast.makeText(this, "Service created...", Toast.LENGTH_LONG).show();      
//   		Log.e(tag, "Service created...");
   		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
   		Timer timer = new Timer();
   		timer.schedule(new TimerTask() {
			
	        @Override
	        public void run() {
	        	Calendar c = Calendar.getInstance();
	        	
	   		 	long lNowTime = c.getTimeInMillis();
				long lLastOpenTime = sharedPreferences.getLong("lastOpenDoorTime", 0);
				long lDiffTime = lNowTime - lLastOpenTime;
				
//				Log.e("Service running", Long.toString(lDiffTime));
				if (lDiffTime > MainActivity.MAX_TIME_READY_OPEN) {
					boolean isReadyDoor = sharedPreferences.getBoolean("isReadyOpenDoor", false);
					if (!isReadyDoor)
						createNotification();
				}
	        }
		}, 0, 1000);
   	}
   	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@SuppressLint("NewApi")
	public void createNotification() {
	    // Prepare intent which is triggered if the
	    // notification is selected
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean("isReadyOpenDoor", true);
		editor.commit();
		
		NotificationCompat.Builder mBuilder =
		        new NotificationCompat.Builder(this)
		        .setSmallIcon(R.drawable.randomdoor)
		        .setContentTitle("Random-Door")
		        .setContentText("Have you checked out the random door today?");
		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(this, MainActivity.class);

		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(MainActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(
		            0,
		            PendingIntent.FLAG_UPDATE_CURRENT
		        );
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager =
		    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		mNotificationManager.notify(10, mBuilder.build());
	}
	
   	@Override
   	public void onStart(Intent intent, int startId) {      
   		super.onStart(intent, startId);  
   		Log.e(tag, "Service started...");
   	}
   	
   	@Override
   	public void onDestroy() {
   		super.onDestroy();
   		Toast.makeText(this, "Service destroyed...", Toast.LENGTH_LONG).show();
   	}

   	@Override
   	public IBinder onBind(Intent intent) {
   		return null;
   	}
}