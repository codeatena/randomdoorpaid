package com.paradymix.randomdoorpaid;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.util.Xml;
import android.widget.ImageView.ScaleType;

class MyAsyncTask extends AsyncTask<String, Integer, Integer>{
	
	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
//	public static final String ACTION_GET_IMAGE = "action_get_image";
	
	public static final String ACTION_IMAGE_BEHIND_DOOR = "image_behind_door";
	public static final String ACTION_IMAGE_RANDOM_MARK = "image_random_mark";
	public static final String ACTION_IMAGE_OPEN_DOOR_ANIMATE = "image_open_door_animate";
	public static final String ACTION_IMAGE_CLOSE_DOOR_ANIMATE = "image_close_door_animate";
	public static final String SECRET_KEY = "73e4d8e848405a88f444cff1c9dbc5b8";
	
    public Context parent;
    public ProgressDialog dlgLoading;
    
    public String curAction;
    
    public boolean isShowProgressDialog;

	public MyAsyncTask(Context _parent, boolean isShow) {
		parent = _parent;
		isShowProgressDialog = isShow;
	}
	
	@Override
	protected Integer doInBackground(String... params) {
		// TODO Auto-generated method stub
		int n = FAIL;
		curAction = params[0];
		
		n = downloadImageData();
		if (n == SUCCESS) {
			
		}
		return n;
	}
	
	@Override
	protected void onPreExecute() {
		if (isShowProgressDialog) {
			dlgLoading = new ProgressDialog(parent);
	    	dlgLoading.setMessage("\tLoading...");
	    	dlgLoading.setCanceledOnTouchOutside(false);
	    	dlgLoading.setCancelable(false);
	        dlgLoading.show();
		}
	}

	protected void onPostExecute(Integer result){
		Log.e("HCW", "post_end");
		
		
		if (!curAction.equals(ACTION_IMAGE_BEHIND_DOOR)) {
			MainActivity mainAcitivity = (MainActivity) parent;
			mainAcitivity.refreshAllImage();
		}
		
		if (isShowProgressDialog) {
			if (dlgLoading.isShowing())
				dlgLoading.dismiss();
		}
//		parent.startActivity(new Intent(parent, CompleteActivity.class));
	}
	
	protected void onProgressUpdate(Integer... progress){
//		Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_LONG).show();
//    	dlgLoading.dismiss();
	}
	
	public Integer downloadImageData() {
		
		try {
			HttpPost httppost = new HttpPost("http://random-door.com/getAPI.php");
	        HttpClient httpclient = GetClient();
			HttpResponse response = null;
						
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	        
	        nameValuePairs.add(new BasicNameValuePair("action", curAction));
	        nameValuePairs.add(new BasicNameValuePair("key", SECRET_KEY));
	        nameValuePairs.add(new BasicNameValuePair("unique-ID", "12345"));

	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			response = httpclient.execute(httppost);
			
			String strBase64 = EntityUtils.toString(response.getEntity());
			
//			Log.e(curAction, strBase64);
			
			byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
			Bitmap bmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
//			MainActivity.bmp = BitmapFactory.decodeStream(new java.net.URL(url).openStream());
			if (curAction.equals(ACTION_IMAGE_RANDOM_MARK)) {
				MainActivity.bmpRandomMark = bmp;
//				MainActivity.refreshAllImage();
			}
			if (curAction.equals(ACTION_IMAGE_BEHIND_DOOR)) {
				MainActivity.bmpBehindDoor = bmp;
			}
			if (curAction.equals(ACTION_IMAGE_OPEN_DOOR_ANIMATE)) {
				MainActivity.bmpOpenDoorAnimate = bmp;
//				MainActivity.refreshAllImage();
//				MainActivity.imgDoorBack.setImageBitmap(bmp);
			}
			if (curAction.equals(ACTION_IMAGE_CLOSE_DOOR_ANIMATE)) {
				MainActivity.bmpCloseDoorAnimate = bmp;
//				MainActivity.imgDoorClosed.setImageBitmap(bmp);
			}
		} catch (Exception e) {
            Log.e("ClientProtocolException", e.toString());
            return FAIL;
        }
		
		return SUCCESS;
	}
	
	public static DefaultHttpClient GetClient() {
		DefaultHttpClient ret = null;

		// sets up parameters
		HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params, "utf-8");
		params.setBooleanParameter("http.protocol.expect-continue", false);

		// registers schemes for both http and https
		SchemeRegistry registry = new SchemeRegistry();
		registry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		registry.register(new Scheme("https", new EasySSLSocketFactory(), 443));
		ThreadSafeClientConnManager manager = new ThreadSafeClientConnManager(
				params, registry);
		ret = new DefaultHttpClient(manager, params);
		return ret;
	}
}
	
