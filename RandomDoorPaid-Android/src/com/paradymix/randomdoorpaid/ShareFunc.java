package com.paradymix.randomdoorpaid;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.util.Log;

public class ShareFunc {
	
	public static boolean share(Context ctx, String nameApp) {
	      try
	      {
	          List<Intent> targetedShareIntents = new ArrayList<Intent>();
	          Intent share = new Intent(android.content.Intent.ACTION_SEND);
	          share.setType("*/*");
	          
	          List<ResolveInfo> resInfo = ctx.getPackageManager().queryIntentActivities(share, 0);
	          if (!resInfo.isEmpty()){
	              for (ResolveInfo info : resInfo) {
	                  Intent targetedShare = new Intent(android.content.Intent.ACTION_SEND);
	                  targetedShare.setType("image/jpeg"); // put here your mime type
	                  if (info.activityInfo.packageName.toLowerCase().contains(nameApp) || info.activityInfo.name.toLowerCase().contains(nameApp)) {
	                      targetedShare.putExtra(Intent.EXTRA_TEXT,"random-door.com");
	                      
	                      ArrayList<Uri> uris = new ArrayList<Uri>();
	                      File dir = Environment.getExternalStorageDirectory();
	                      File yourFile = new File(dir, "randomdoor.png");
//	                      uris.add(Uri);
	                      targetedShare.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(yourFile));
	                      targetedShare.setPackage(info.activityInfo.packageName);
//	    	              startActivity(targetedShare);
	                      targetedShare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
	                      ctx.startActivity(Intent.createChooser(targetedShare, "Share image using"));
	                      targetedShareIntents.add(targetedShare);
	                  }
	              }
	              Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Select app to share");
	              chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
	              
	              return true;
//	              startActivity(chooserIntent);
	          }
	      }
	      catch(Exception e){
	    	  
	          Log.e("VM","Exception while sending image on" + nameApp + " "+  e.getMessage());
	          return false;
	      }
	      
	      return false;
	}
	
    public static void showGeneralAlert(Context context, String title, String message) {
     	new AlertDialog.Builder(context)
     	
		 .setTitle(title)
		 .setMessage(message)
		 .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		 public void onClick(DialogInterface dialog, int which) { 
		     // continue with delete
		     }
		  })
		  .show();
     }
}
