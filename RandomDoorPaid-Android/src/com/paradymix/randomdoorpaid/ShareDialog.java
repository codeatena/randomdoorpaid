package com.paradymix.randomdoorpaid;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ShareDialog extends Dialog {
	Activity parent;

	Button btnFacebook;
	Button btnTwitter;
	Button btnInstagram;
	Button btnBack;
	
	ImageView imgClose;
	
//	private AdView adView;
	
	public ShareDialog(Context context, String title) {
		super(context);
		// TODO Auto-generated constructor stub
		parent = (Activity) context;
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.dialog_share);
		setCanceledOnTouchOutside(false);
		setTitle(title);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		btnFacebook = (Button) findViewById(R.id.share_facebook_button);
		btnTwitter = (Button) findViewById(R.id.share_twitter_button);
		btnInstagram = (Button) findViewById(R.id.share_instagram_button);
		btnBack = (Button) findViewById(R.id.back_button);
		
//		LinearLayout llytAdView = (LinearLayout) findViewById(R.id.adview_linearLayout);
//		adView = new AdView(parent, AdSize.BANNER, "ca-app-pub-5533489369819223/9642874793");;
//		AdRequest re = new AdRequest();
//		re.setTesting(true);
//		adView.loadAd(re);
//		
//		llytAdView.addView(adView);
		
		imgClose= (ImageView) findViewById(R.id.close_image);
	}
	
	private void initValue() {
		
	}
	
	private void initEvent() {
		btnFacebook.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!ShareFunc.share(parent, "facebook")) {
					ShareFunc.showGeneralAlert(parent, "Information!", "Your phone doesnt have the facebook app installed. " +
							"Download the facebook app to share random-door on facebook.");
				}
				ShareDialog.this.dismiss();
			}
        });
		
		btnTwitter.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!ShareFunc.share(parent, "twitter")) {
					ShareFunc.showGeneralAlert(parent, "Information!", "Your phone doesnt have the twitter app installed. " +
							"Download the Twitter app to share random-door on twitter.");
				}
				ShareDialog.this.dismiss();
			}
        });
		
		btnInstagram.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!ShareFunc.share(parent, "instagram")) {
					ShareFunc.showGeneralAlert(parent, "Information!", "Your phone doesnt have the instagram app installed. " +
							"Download the instagram app to share random-door on instagram.");
				}
				ShareDialog.this.dismiss();
			}
        });
		
		btnBack.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				FullActivity fullActivity = (FullActivity) parent;
				ShareDialog.this.dismiss();
				fullActivity.finish();
			}
        });
		
		imgClose.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				ShareDialog.this.dismiss();
			}
        });
	}
}
