package com.paradymix.randomdoorpaid;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.ads.*;

@SuppressLint("NewApi")
public class MainActivity extends Activity {

	static long MAX_TIME_READY_OPEN = 24 * 3600 * 1000;
	private static int FULL_ACTIVITY_REQUEST_CODE = 1001;
	
	static int READY_TIME_OPEN = 0;
	
	private static int CLOSE_ANIMATION_FRAME_COUNT = 9;
	private static int OPEN_ANIMATION_FRAME_COUNT = 9;

//	static long MAX_TIME_READY_OPEN = 5 * 60 * 1000;
	long lLastOpenTime;
	
	static int DELAY_TIME_CLOSE = 3000;
	
	static int DURATION_TIME_CLOSE = 900;
	static int DURATION_TIME_OPEN = 1800;

	static String MY_AD_UNIT_ID = "a15070abc92c05e";
	
	public static Bitmap bmpRandomMark = null;
	public static Bitmap bmpBehindDoor = null;
	public static Bitmap bmpBehindDoorTemp = null;

	public static Bitmap bmpOpenDoorAnimate = null;
	public static Bitmap bmpCloseDoorAnimate = null;

	Bitmap bmpReadyDoor = null;
	Bitmap bmpLockedDoor = null;
	
	AnimationDrawable openAnimation;
	AnimationDrawable closeAnimation;
	
	Animation animZoomIn;
	Animation animZoomOut;
	/*                         facebook                     */
//	private static String APP_ID = "585449168213280"; // Replace with your App ID
//	private Facebook facebook = new Facebook(APP_ID);
//	private AsyncFacebookRunner mAsyncRunner;
//	String FILENAME = "AndroidSSO_data";
//	private SharedPreferences mPrefs;
	/*                         facebook                     */
	
	public static ImageView imgDoorBack;
//	public static ImageView imgDoorClosed;
	public static ImageView imgDoorOpened;
	public static ImageView imgRandomDoor;
	
	Button btnFacebook;
	Button btnTwitter;
	Button btnInstagram;
	
	TextView txtReadyTime;
	
	RelativeLayout Rltcontainer;
	
	LinearLayout llytReadyText;
	
	boolean isOpenedDoor;
	boolean isLockedDoor;
	
	public SharedPreferences sharedPreferences;
	
//	public static AdView adView;
	Timer timer = null;
	Timer timer1 = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
        startService(new Intent(this, MyService.class));
		
		initWidget();
		initValue();
		initEvent();
		
		if (isNetworkOnline()) {
			Log.e("Network Status", "true");
		} else {
			Log.e("Network Status", "false");
			ShareFunc.showGeneralAlert(MainActivity.this, "Network failure!", "Sorry but it appears that Random-Door can't access the internet " +
					"at this time. Please check your connection and try again later.");
			
			return;
		}
		
		if (timer != null) {
			timer.cancel();
			timer = null;
		}

		timer = new Timer();
		timer.schedule(new TimerTask() {
			
	        @Override
	        public void run() {
	        	calcReadyDoor();
	        }
		}, 0, 1000);
		
		if (timer1 != null) {
			timer1.cancel();
			timer1 = null;
		}
		timer1 = new Timer();
		timer1.schedule(new TimerTask() {
			
	        @Override
	        public void run() {

				new MyAsyncTask(MainActivity.this, false).execute(MyAsyncTask.ACTION_IMAGE_BEHIND_DOOR);
	        }
		}, 0, 3000);
		
	}
	
   	public void onDestroy() {
   		super.onDestroy();
   		timer1.cancel();
   		timer.cancel();
   	}
   	
	private void calcReadyDoor() {
		
		this.runOnUiThread(new Runnable() {
			public void run() {
				lLastOpenTime = sharedPreferences.getLong("lastOpenDoorTime", 0);
				long lDiffTime = getTimeNowDiffLastSubmit(); 
				
		    	if (lDiffTime < READY_TIME_OPEN * 1000) {
		    		
//		    		Log.e("isOpenedDoor", "true");
//		    		isOpenedDoor = true;
		    		int k = 3600 * 24 - (int) lDiffTime / 1000;
		    		
		    		int h = k / 3600;
		    		int m = (k - h * 3600) / 60;
		    		int s = k % 60;
//		    		txtReadyTime.sestVisibility(View.VISIBLE);
//		    		txtReadyTime.setText("Closed!!!\nCheck back in \n" + Integer.toString(h) + ":" + Integer.toString(m) + ":" + Integer.toString(s));
		    		
		    		int nShowTime = (int) (READY_TIME_OPEN - lDiffTime / 1000);
		    		txtReadyTime.setText(Integer.toString(nShowTime));
		    		
		    		isLockedDoor = true;
		    		
//		    		if (nShowTime < 1) {
//		    			llytReadyText.setVisibility(View.GONE);
//						imgDoorBack.setImageBitmap(bmpLockedDoor);
//						isOpenedDoor = false;
//		    		}
		    	} else {
//		    		Log.e("isOpenedDoor", "false");
//		    		txtReadyTime.setText("the door is ready for you again.");
		    		llytReadyText.setVisibility(View.GONE);
//		    		isOpenedDoor = false;
		    		isLockedDoor = false;
//		    		isOpenedDoor = false;
//		    		if (!isOpenedDoor) {
//			    		imgDoorBack.setImageBitmap(bmpReadyDoor);
//		    		}
//		    		imgDoorClosed.setImageResource(R.drawable.door);
		    	}
			}
		});
	}

	private void initWidget() {
		
		imgDoorBack = (ImageView) findViewById(R.id.door_back_imageView);
//		imgDoorClosed = (ImageView) findViewById(R.id.door_close_imageView);
		imgDoorOpened = (ImageView) findViewById(R.id.door_open_imageView);
		imgRandomDoor = (ImageView) findViewById(R.id.random_door_imageView);
		
		Rltcontainer = (RelativeLayout) findViewById(R.id.container_relativeLayout);
		
		btnFacebook = (Button) findViewById(R.id.facebook_button);
		btnTwitter = (Button) findViewById(R.id.twitter_button);
		btnInstagram = (Button) findViewById(R.id.instagram_button);
		
		txtReadyTime = (TextView) findViewById(R.id.ready_time_textView);
		
		llytReadyText = (LinearLayout) findViewById(R.id.ready_linearLayout);
		
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;
		
		float scale = (float) height / (float) 400.0f;
		Log.e("scale", Float.toString(scale));
		
		android.view.ViewGroup.LayoutParams layoutParams = imgDoorBack.getLayoutParams();
		layoutParams.width = (int) ((float) 149.0f * scale);
		layoutParams.height = (int) ((float) 265.0f * scale);
		imgDoorBack.setLayoutParams(layoutParams);
		
//		layoutParams = imgDoorClosed.getLayoutParams();
//		layoutParams.width = (int) ((float) 118.0f * scale);
//		layoutParams.height = (int) ((float) 250.0f * scale);
//		imgDoorClosed.setLayoutParams(layoutParams);
//		imgDoorClosed.setScaleType(ScaleType.FIT_XY);

		layoutParams = imgDoorOpened.getLayoutParams();
		layoutParams.width = (int) ((float) 118.0f * scale);
		layoutParams.height = (int) ((float) 250.0f * scale);
		imgDoorOpened.setLayoutParams(layoutParams);
		imgDoorOpened.setScaleType(ScaleType.FIT_XY);
		
		RelativeLayout.LayoutParams relativeLayoutParams = (RelativeLayout.LayoutParams) llytReadyText.getLayoutParams();
		int top = (int) ((float) 50.0f * scale);
		relativeLayoutParams.setMargins(0, top, 0, 0);
//		relativeLayoutParams.width = (int) ((float) 90.0f * scale);
//		txtReadyTime.setTextSize((int) (9.0f * (scale + 1.0f) / 2.0f));
		llytReadyText.setLayoutParams(relativeLayoutParams);
		
		animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
		animZoomOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
	}
	
	public boolean isNetworkOnline() {
	    boolean status=false;
	    try{
	        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	        NetworkInfo netInfo = cm.getNetworkInfo(0);
	        if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
	            status= true;
	        }else {
	            netInfo = cm.getNetworkInfo(1);
	            if(netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED)
	                status= true;
	        }
	    }catch(Exception e){
	        e.printStackTrace();  
	        return false;
	    }
	    return status;

	}  
	
	public void refreshAllImage() {
		imgRandomDoor.setImageBitmap(bmpRandomMark);
		
		if (bmpOpenDoorAnimate != null) {
			bmpReadyDoor = Bitmap.createBitmap(bmpOpenDoorAnimate, 0, 0, 298, 531);
//			imgDoorBack.setImageBitmap(bmp);
//			imgDoorBack.setBackgroundResource(R.drawable.door_back);
			if (!isLockedDoor) {
				imgDoorBack.setImageBitmap(bmpReadyDoor);
			}
			loadOpenAnimation();
		}
		
		if (bmpCloseDoorAnimate != null) {
			bmpLockedDoor = Bitmap.createBitmap(bmpCloseDoorAnimate, 0, 0, 298, 531);
			if (isLockedDoor) {
				imgDoorBack.setImageBitmap(bmpLockedDoor);
			}
			loadCloseAnimation();
		}
		
//		Animation anim = new Animation();

	}
	
	public void loadOpenAnimation() {
		openAnimation = new AnimationDrawable();
		
		for (int i = 0; i < OPEN_ANIMATION_FRAME_COUNT; i++) {
			Bitmap bmp = Bitmap.createBitmap(bmpOpenDoorAnimate, i * 298, 0, 298, 531);
			Drawable d = new BitmapDrawable(bmp);
			openAnimation.addFrame(d, 100);
		}
		
//		imgDoorBack.setImageDrawable(openAnimation);
	}
	
	public void loadCloseAnimation() {
		closeAnimation = new AnimationDrawable();
		for (int i = CLOSE_ANIMATION_FRAME_COUNT - 1; i >= 0; i--) {
			Bitmap bmp = Bitmap.createBitmap(bmpCloseDoorAnimate, i * 298, 0, 298, 531);
			Drawable d = new BitmapDrawable(bmp);
			closeAnimation.addFrame(d, 100);
		}
	}
	
	private void initValue() {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		
		SoundManager.getInstance();
		SoundManager.initSounds(this);
		SoundManager.loadSounds();
		
		isOpenedDoor = false;
		
//		mAsyncRunner = new AsyncFacebookRunner(facebook);
//		
//		Bitmap bm = BitmapFactory.decodeResource( getResources(), R.drawable.randomdoor);
//		String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
//		try {
//			File file = new File(extStorageDirectory, "randomdoor.png");
//			FileOutputStream outStream = new FileOutputStream(file);
//		    bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
//		    outStream.flush();
//		    outStream.close();
//		} catch (Exception e) {
//			
//		}
		
		
//		bmpBehindDoor = BitmapFactory.decodeResource(getResources(), R.drawable.lion);

//		imgDoorOpened.setVisibility(View.GONE);
//		imgDoorClosed.setVisibility(View.GONE);
	}
	
	public void writeBehindDoorImage() {
		String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		Bitmap bmp = null;
		
		if (bmpBehindDoorTemp == null) {
			bmp = bmpBehindDoor;
		} else {
			bmp = bmpBehindDoorTemp;
		}
		
		if (bmp == null) return;
		try {
			File file = new File(extStorageDirectory, "randomdoor.png");
			FileOutputStream outStream = new FileOutputStream(file);
			bmp.compress(Bitmap.CompressFormat.PNG, 100, outStream);
		    outStream.flush();
		    outStream.close();
		} catch (Exception e) {
			
		}
	}
	
	private void initEvent() {
		
		btnFacebook.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
//				SoundManager.playSound(SoundManager.DOOR_OPEN, 1);
				writeBehindDoorImage();
				File filePath = MainActivity.this.getApplicationContext().getFileStreamPath("photo.jpg");
				if (!ShareFunc.share(MainActivity.this, "facebook")) {
					ShareFunc.showGeneralAlert(MainActivity.this, "Information!", "Your phone doesnt have the facebook app installed. " +
							"Download the facebook app to share random-door on facebook.");
				}
			}
        });
		
		btnTwitter.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
//				SoundManager.playSound(SoundManager.DOOR_SHUT, 1);
				writeBehindDoorImage();
				if (!ShareFunc.share(MainActivity.this, "twitter")) {
					ShareFunc.showGeneralAlert(MainActivity.this, "Information!", "Your phone doesnt have the twitter app installed. " +
							"Download the Twitter app to share random-door on twitter.");
				}
			}
        });
		
		btnInstagram.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
//				SoundManager.playSound(SoundManager.DOOR_LOCK, 1);
				writeBehindDoorImage();
				if (!ShareFunc.share(MainActivity.this, "instagram")) {
					ShareFunc.showGeneralAlert(MainActivity.this, "Information!", "Your phone doesnt have the instagram app installed. " +
							"Download the instagram app to share random-door on instagram.");
				}
			}
        });
		
		imgDoorBack.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
//					new MyAsyncTask(MainActivity.this).execute(MyAsyncTask.ACTION_GET_IMAGE);

				if (isLockedDoor || isOpenedDoor) {
					shrinkAnimation();
					return;
				}
				
				if (bmpBehindDoor != null) {
					imgDoorOpened.setImageBitmap(bmpBehindDoor);
					bmpBehindDoorTemp = bmpBehindDoor;
				}
				
				writeBehindDoorImage();
				openDoorAnimation();

				Timer timer = new Timer();
				timer.schedule(new TimerTask() {
			        @Override
			        public void run() {
	        			MainActivity.this.startActivityForResult(new Intent(MainActivity.this, FullActivity.class), 
	        					FULL_ACTIVITY_REQUEST_CODE);
			        }
				}, DELAY_TIME_CLOSE);
				
				Timer timer1 = new Timer();
				timer1.schedule(new TimerTask() {
			        @Override
			        public void run() {
			        	//showReadyTime();
			        }
				}, DELAY_TIME_CLOSE + DURATION_TIME_CLOSE);
			}
        });
		
		new MyAsyncTask(MainActivity.this, true).execute(MyAsyncTask.ACTION_IMAGE_RANDOM_MARK);
		new MyAsyncTask(MainActivity.this, true).execute(MyAsyncTask.ACTION_IMAGE_OPEN_DOOR_ANIMATE);
		new MyAsyncTask(MainActivity.this, true).execute(MyAsyncTask.ACTION_IMAGE_CLOSE_DOOR_ANIMATE);
	}
	
	private void openDoorAnimation() {
		SoundManager.playSound(SoundManager.DOOR_OPEN, 1);

		isOpenedDoor = true;

		imgDoorBack.setImageDrawable(openAnimation);
		openAnimation.stop();
//		openAnimation.selectDrawable(0);
		openAnimation.start();
	}
	
	private void closeDoorAnimation() {
		SoundManager.playSound(SoundManager.DOOR_SHUT, 1);

		this.runOnUiThread(new Runnable() {
			public void run() {
//				final float centerX = 0;
//				final float centerY = imgDoorClosed.getHeight() / 2;
//				imgDoorClosed.setImageResource(R.drawable.door_complete);
//				final Flip3dAnimation rotation = new Flip3dAnimation(90, 0, centerX, centerY);
//			
//				rotation.setDuration(DURATION_TIME_CLOSE);
//				rotation.setFillAfter(true);
//			
//				imgDoorClosed.startAnimation(rotation);
//				closeAnimation
				closeAnimation.stop();
				imgDoorBack.setImageDrawable(closeAnimation);
				closeAnimation.start();
			}
		});
	}
	
	private void showReadyTime() {
		Log.e("hcw", "showReadyTime");
//		new MyAsyncTask(MainActivity.this).execute(MyAsyncTask.ACTION_GET_IMAGE);
		isOpenedDoor = false;
		this.runOnUiThread(new Runnable() {
			public void run() {
//				llytReadyText.setVisibility(View.VISIBLE);
				imgDoorBack.setImageBitmap(bmpReadyDoor);
			}
		});
//		new MyAsyncTask(MainActivity.this).execute(MyAsyncTask.ACTION_GET_IMAGE);

	}
	
	public long getTimeNowDiffLastSubmit() {
		Calendar c = Calendar.getInstance();
		long lNowTime = c.getTimeInMillis();
		long lDiffTime = lNowTime - lLastOpenTime;
		return lDiffTime;
	}
	
	public void shrinkAnimation() {
		
		SoundManager.playSound(SoundManager.DOOR_LOCK, 1);
		Animation animation;
		animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move);
		Rltcontainer.startAnimation(animation);
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == FULL_ACTIVITY_REQUEST_CODE) {
			
			 Calendar c = Calendar.getInstance();
			 
			 SharedPreferences.Editor editor = sharedPreferences.edit();
			 long lNowTime = c.getTimeInMillis();
			 editor.putLong("lastOpenDoorTime", lNowTime);
			 editor.putBoolean("isReadyOpenDoor", false);
			 editor.commit();
			 
			closeDoorAnimation();
			
			Timer timer1 = new Timer();
			timer1.schedule(new TimerTask() {
		        @Override
		        public void run() {
		        	showReadyTime();
		        }
			}, DURATION_TIME_CLOSE);
		}
	}
}